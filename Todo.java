//Author : Siddharth Mohanty
//Email : siddharthmohantywk@gmail.com
//Contact/WhatsApp : +91 9967680280

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.*;

public class Todo {

	private static void printHelp() {
		System.out.println("Usage :-\n"+
    "$ ./todo add \"todo item\"  # Add a new todo\n"+
    "$ ./todo ls               # Show remaining todos\n"+
    "$ ./todo del NUMBER       # Delete a todo\n"+
    "$ ./todo done NUMBER      # Complete a todo\n"+
    "$ ./todo help             # Show usage\n"+
    "$ ./todo report           # Statistics");}

	private static void printEventList(FileWriter file) throws IOException {
		LineNumberReader lineNoObj = new LineNumberReader(new FileReader("todo.txt"));
		String eventList="";
		String line;
		while(true) {
			//Get the line to be replaced
			line = lineNoObj.readLine();
			if (line == null) {
				break;
			}

			
			eventList = "[" + lineNoObj.getLineNumber() + "] " + line + System.lineSeparator() + eventList;
		}

		System.out.println(eventList);
		file.close();
		lineNoObj.close();
	}

	public static void main(String args[]) throws IOException, NullPointerException {

		
		LocalDate today = LocalDate.now();		
		FileWriter todoFile = new FileWriter("todo.txt", true);
		FileWriter doneFile = new FileWriter("done.txt", true);

		if(args.length == 0) {
			printHelp();
		}

		if(args.length == 1) {
			if(args[0].equals("help")) {
				printHelp();
			}

			if(args[0].equals("ls")) {
				LineNumberReader lineNumberReader = new LineNumberReader(new FileReader("todo.txt"));
				lineNumberReader.skip(Long.MAX_VALUE);
				int pending = lineNumberReader.getLineNumber();
				if(pending==0) {
					System.out.println("There are no pending todos!");
				}
				else
					printEventList(todoFile);
				lineNumberReader.close();
			}

			if(args[0].equals("done")) {
				System.out.println("Error: Missing NUMBER for marking todo as done.");
			}

			if(args[0].equals("del")) {
				System.out.println("Error: Missing NUMBER for deleting todo.");
			}

			if(args[0].equals("add")) {
				System.out.println("Error: Missing todo string. Nothing added!");
			}

		}

		if(args.length > 1) {
			if(args[0].equals("add")) {
				String item = args[1];
				System.out.println("Added todo: "+ "\"" + item + "\"");
				
				todoFile.write(item + System.lineSeparator());
			}

			if(args[0].equals("done") || args[0].equals("del")) {
				int lineNo = Integer.parseInt(args[1]);
				String line="";
				
				LineNumberReader lineNoObj = new LineNumberReader(new FileReader("todo.txt"));
				StringBuffer buffer = new StringBuffer();
				while(true) {
					//Get the line to be replaced
					
					line = lineNoObj.readLine();
					if (line == null) {
						break;
					}

					if(lineNoObj.getLineNumber() != lineNo) {
						buffer.append(line + System.lineSeparator());
					}

					if (args[0].equals("done")) {
						//
						LineNumberReader lineNumberReader = new LineNumberReader(new FileReader("todo.txt"));
						lineNumberReader.skip(Long.MAX_VALUE);
						int pending = lineNumberReader.getLineNumber();
						if (lineNo > pending || lineNo <= 0) {
							System.out.println("Error: todo #" + lineNo + " does not exist.");
							break;
						}
						lineNumberReader.close();
					}

					if (args[0].equals("del")) {
						//
						LineNumberReader lineNumberReader = new LineNumberReader(new FileReader("todo.txt"));
						lineNumberReader.skip(Long.MAX_VALUE);
						int pending = lineNumberReader.getLineNumber();
						if (lineNo > pending || lineNo <= 0) {
							System.out.println("Error: todo #" + lineNo + " does not exist. Nothing deleted.");
							break;
						}
						else {
							System.out.println("Deleted todo #"+lineNo);
						}
						lineNumberReader.close();
						
					}

					if(args[0].equals("done") && lineNoObj.getLineNumber() == lineNo) {
						System.out.println("Marked todo #" + lineNo + " as done.");
						doneFile.write(line + System.lineSeparator());
					}
				}
				lineNoObj.close();
				String fileContents = buffer.toString();
				todoFile = new FileWriter("todo.txt");
				todoFile.write(fileContents);
			}
		}
	if(args.length == 1)
	{
		if (args[0].equals("report")) {
			LineNumberReader lineNumberReader = new LineNumberReader(new FileReader("todo.txt"));
			lineNumberReader.skip(Long.MAX_VALUE);
			int pending = lineNumberReader.getLineNumber();
			
			lineNumberReader = new LineNumberReader(new FileReader("done.txt"));
			lineNumberReader.skip(Long.MAX_VALUE);
			int completed = lineNumberReader.getLineNumber();
			lineNumberReader.close();

			System.out.println(today.toString() + " Pending : " + pending + " Completed : " + completed);

		}
	}
	todoFile.close();
	doneFile.close();
	}
}
